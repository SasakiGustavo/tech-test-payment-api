using tech_test_payment_api.Entities;
using Microsoft.EntityFrameworkCore;

namespace tech_test_payment_api.Context
{
    public class PottencialContext : DbContext
    {
        public PottencialContext(DbContextOptions<PottencialContext> options) : base(options)
        {
            
        }

        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<ItemVenda> ItensVenda { get; set; }
        public DbSet<Produto> Produtos { get; set; }

    }
}