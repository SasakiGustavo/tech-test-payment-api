
namespace tech_test_payment_api.Entities
{
    public class Venda
    {
        public int Id { get; set; }
        public int IdVendedor { get; set; }
        public DateTime Data { get; set; }
        public List<ItemVenda> ItensVenda { get; set; }
        public StatusVenda Status { get; set; }
    }
}