
using Microsoft.EntityFrameworkCore;

namespace tech_test_payment_api.Entities
{
    public class ItemVenda
    {
        public int Id { get; set; }
        public int IdVenda { get; set; }
        public int IdProduto { get; set; }
        public int Quantidade { get; set; }
        
    }

}