using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;
using Microsoft.AspNetCore.Mvc;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly PottencialContext _context;

        public VendedorController(PottencialContext context)
        {
            _context = context;
        }

        [HttpPost("CadastrarVendedor")]
        public IActionResult Create(string nome, string cpf, string email, string telefone)
        {
            Vendedor vendedor = new Vendedor(nome, cpf, email, telefone);
            _context.Add(vendedor);
            _context.SaveChanges();

            return Ok(vendedor);
        }

        [HttpGet("ListarVendedores")]
        public IActionResult Read()
        {
            var vendedor = _context.Vendedores.ToList();

            if(vendedor == null) return NotFound();

            return Ok(vendedor);
        }

        [HttpPut("AtualizarVendedor/{id}")]
        public IActionResult Update(int id, Vendedor update)
        {
            var vendedor = _context.Vendedores.Find(id);

            if(vendedor == null) return NotFound();

            vendedor.Nome = update.Nome;
            vendedor.Cpf = update.Cpf;
            vendedor.Email = update.Email;
            vendedor.Telefone = update.Telefone;

            _context.Vendedores.Update(vendedor);
            _context.SaveChanges();

            return Ok(vendedor);
        }

        [HttpDelete("DeletarVendedor/{id}")]
        public IActionResult Delete(int id)
        {
            var vendedor = _context.Vendedores.Find(id);

            if(vendedor == null) return NotFound();

            _context.Vendedores.Remove(vendedor);
            _context.SaveChanges();

            return NoContent();
        }
    }
}