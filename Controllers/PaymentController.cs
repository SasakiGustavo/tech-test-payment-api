using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;
using Microsoft.AspNetCore.Mvc;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PaymentController : ControllerBase
    {
        private readonly PottencialContext _context;

        public PaymentController(PottencialContext context)
        {
            _context = context;
        }

        [HttpPost("RegistrarVenda")]
        public IActionResult RegistrarVenda(int idVendedor, List<ItemVendaNovo> itensVenda)
        {
            Venda venda = new Venda();
            venda.ItensVenda = new List<ItemVenda>();

            if(_context.Vendedores.Find(idVendedor) == null) return NotFound("Vendedor não encontrado");

            venda.IdVendedor = idVendedor;
            venda.Data = DateTime.Now;
            foreach(var add in itensVenda)
            {
                if(_context.Produtos.Find(add.IdProduto) == null) throw new("Produto não cadastrado");
                ItemVenda item = new ItemVenda();
                item.IdProduto = add.IdProduto;
                item.Quantidade = add.Quantidade;
                venda.ItensVenda.Add(item);
            }
            venda.Status = 0;

            _context.Add(venda);
            _context.SaveChanges();

            foreach(var item in venda.ItensVenda)
            {
                item.IdVenda = venda.Id;
                _context.ItensVenda.Update(item);
                _context.SaveChanges();
            }

            return Ok(venda);
        }

        [HttpGet("BuscarVendaPorId/{id}")]
        public IActionResult BuscarVendaPorId(int id)
        {
            var venda = _context.Vendas.Find(id);

            if(venda == null) return NotFound();

            venda.ItensVenda = _context.ItensVenda.Where(x => x.IdVenda == id).ToList();

            return Ok(venda);
        }

        [HttpPut("AtualizarStatusVenda/{id}, {status}")]
        public IActionResult AtualizarStatusVenda(int id, StatusVenda status)
        {
            var venda = _context.Vendas.Find(id);

            if(venda == null) return NotFound();

            venda.ItensVenda = _context.ItensVenda.Where(x => x.IdVenda == id).ToList();

            if(venda.Status == StatusVenda.AguardandoPagamento && (status == StatusVenda.PagamentoAprovado || status == StatusVenda.Cancelada))
            {
                venda.Status = status;
            }
            else if(venda.Status == StatusVenda.PagamentoAprovado && (status == StatusVenda.EnviadoParaTransportadora  || status == StatusVenda.Cancelada))
            {
                venda.Status = status;
            }
            else if(venda.Status == StatusVenda.EnviadoParaTransportadora && status == StatusVenda.Entregue)
            {
                venda.Status = status;
            }
            else
            {
                throw new ("Operação Inválida");
            }

            _context.Vendas.Update(venda);
            _context.SaveChanges();

            return Ok(venda);
        }
    }
}