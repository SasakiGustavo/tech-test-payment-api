using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;
using Microsoft.AspNetCore.Mvc;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProdutoController : ControllerBase
    {
        private readonly PottencialContext _context;

        public ProdutoController(PottencialContext context)
        {
            _context = context;
        }

        [HttpPost("CadastrarProduto")]
        public IActionResult Create(string nome)
        {
            Produto produto = new Produto();
            produto.Nome = nome;
            _context.Add(produto);
            _context.SaveChanges();

            return Ok(produto);
        }

        [HttpGet("ListarProdutos")]
        public IActionResult Read()
        {
            var produto = _context.Produtos.ToList();

            if(produto == null) return NotFound();

            return Ok(produto);
        }

        [HttpPut("AtualizarProduto/{id}")]
        public IActionResult Update(int id, Produto update)
        {
            var produto = _context.Produtos.Find(id);

            if(produto == null) return NotFound();

            produto.Nome = update.Nome;

            _context.Produtos.Update(produto);
            _context.SaveChanges();

            return Ok(produto);
        }

        [HttpDelete("DeletarProduto/{id}")]
        public IActionResult Delete(int id)
        {
            var produto = _context.Produtos.Find(id);

            if(produto == null) return NotFound();

            _context.Produtos.Remove(produto);
            _context.SaveChanges();

            return NoContent();
        }
    }
}